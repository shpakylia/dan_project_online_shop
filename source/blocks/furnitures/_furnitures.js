let products = [
    {
        id: 1,
        title: 'Aenean Ru Bristique',
        img: 'images/products/furniture/1.png',
        stars: 3,
        price: "$30.00",
        old_price: "$45.00",
        reviews: 0,
        favorite: true,
        desc: 'Nam tristique porta ligula, vel viverra sem eleifend nec. Nulla sed purus augue, eu euis mod tellus. Nam mattis eros tis sagittis. Vestibulum suscipit cursus biben.',
        availability: 'in stock',
        category: 'furniture'

    },
    {
        id: 1,
        title: 'Aenean Ru Bristique',
        img: 'images/products/furniture/1.png',
        stars: 3,
        price: "$30.00",
        old_price: "",
        reviews: 0,
        favorite: true,
        desc: 'Nam tristique porta ligula, vel viverra sem eleifend nec. Nulla sed purus augue, eu euis mod tellus. Nam mattis eros tis sagittis. Vestibulum suscipit cursus biben.',
        availability: 'in stock',
        category: 'furniture'

    }
];
const block = document.querySelector('.furniture-items');

products.forEach((product)=>{
    block.innerHTML += `
        <div class="col-12 col-md-6 col-xl-4 d-flex furniture-item">
            <div class="card furniture-item--card">
                <div class="furniture-item--card_head">
                    <button class="btn btn-sale btn-dark">sale</button>
                    <img src="${product.img}" alt="product1">
                    <button class="btn btn-dark btn-view">Quick View</button>
                    <button class="btn btn-dark btn-card"><i class="fa fa-shopping-basket card-icon"></i>ADD TO CARD</button>
                </div>
                <div class="card-body">
                    <p class="furniture-item--card_title">Aenean Ru Bristique</p>
                    <div class="furniture-item--card_rating d-flex justify-content-center ">
                        <span ${product.stars >= 1 ? 'class="active"' : '' } data-star="1"><i class="fa fa-star"></i></span>
                        <span ${product.stars >= 2 ? 'class="active"' : '' } data-star="2"><i class="fa fa-star"></i></span>
                        <span ${product.stars >= 3 ? 'class="active"' : '' } data-star="3"><i class="fa fa-star"></i></span>
                        <span ${product.stars >= 4 ? 'class="active"' : '' } data-star="4"><i class="far fa-star"></i></span>
                        <span ${product.stars >= 5 ? 'class="active"' : '' } data-star="5"><i class="far fa-star"></i></span>
                    </div>
                    <div class="furniture-item--card_price-block">
                        <div class="float-left">
                            <button class="btn btn-favorite"><i class="far fa-heart"></i></button>
                            <button class="btn btn-compare"><i class="fa fa-exchange-alt"></i></button>
                        </div>
                        <div class="float-right">
                            ${product.old_price ? '<span class="old-price">'+product.old_price+'</span>' : ''}
                            <button class="btn btn-dark btn-price">
                                ${product.price}
                            </button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    `;
});
