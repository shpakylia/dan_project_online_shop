import gulp from 'gulp';
import sass from 'gulp-sass';
import uglify from 'gulp-uglify-es';
import concat from 'gulp-concat';
import template from 'gulp-template-html';

gulp.task('html', function () {
    return gulp.src('./source/blocks/**/*.html')
        .pipe(concat('index.html'))
        .pipe(template('./source/layout.html'))
        .pipe(gulp.dest('dist'));
});


gulp.task('sass', function () {

    return gulp.src('source/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});


gulp.task('styles:vendor', function () {
    return gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.css',
        './node_modules/@fortawesome/fontawesome-free/css/fontawesome.css'
        ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function () {
    return gulp.src(
        [
            './source/blocks/**/*.js',
            './source/main.js'

        ]
    )
        .pipe(uglify())
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'))
});


gulp.task('scripts:vendor', function () {

    return gulp.src(
        [
            './node_modules/jquery/dist/jquery.js',
            './node_modules/@fortawesome/fontawesome-free/js/all.js',
            './node_modules/bootstrap/dist/js/bootstrap.js',

        ]
    )
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('dist/js'))
});


gulp.task('watch:html', function () {
    gulp.watch('source/blocks/**/*.html', gulp.series('html'));
});

gulp.task('watch:sass', function () {
    gulp.watch(
        [
        'source/blocks/**/*.sass',
        'source/includes/*.sass'
        ], gulp.series('sass'));
});

gulp.task('watch:script', function () {
    gulp.watch('source/blocks/**/*.js', gulp.series('scripts'));
});

gulp.task('watch', gulp.parallel('watch:html', 'watch:script', 'watch:sass'));


gulp.task('default', gulp.parallel('html', 'sass', 'scripts', 'styles:vendor', 'scripts:vendor'));